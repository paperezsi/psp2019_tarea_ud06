
# Tarea para PSP06.
La tarea de la unidad esta dividida en dos actividades.

## Actividad 6.1.
Crea una aplicación que realice los siguientes pasos:
Solicita el nombre del usuario que va a utilizar la aplicación. El login tiene una longitud de 8 caracteres y está compuesto únicamente por letras minúsculas.
Solicita al usuario el nombre de un fichero que quiere mostrar. El nombre del fichero es como máximo de 8 caracteres y tiene una extensión de 3 caracteres.
Visualiza en pantalla el contenido del fichero.

Es importante tener en cuenta que se tiene que realizar una validación de los datos de entrada y llevar un registro de la actividad del programa.

## Instalación

Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp2019_tarea_ud06.git
```

## **Descripción y pruebas del ejercicio 6.1.**

Para la resolución de la tarea, creo un nuevo proyecto Java, con una clase donde hago uso de expresiones regulares para la validación de datos tanto del nombre del usuario como del nombre del fichero.

En la misma clase tambien creo métodos para crear un fichero de Logs y para visualizar y abrir el fichero con la aplicación por defecto del sistema.

![paso1.PNG](readme_src/paso1.PNG)

![paso2.PNG](readme_src/paso2.PNG)

![paso3.PNG](readme_src/paso3.PNG)

La prueba de que los patrones de las expresiones regulares funciona la realizo a través de la introducción de datos por consola.También confirmo la existencia de los ficheros de Log en la raiz del proyecto.

![paso4.PNG](readme_src/paso4.PNG)


## Actividad 6.2.
Utilizando la aplicación desarrollada en la actividad anterior, configura las políticas de acceso para:

- Firmar digitalmente la aplicación.
- Que sólo pueda leer los datos del directorio c:/datos.

## Instalación

Continua siendo la misma rama que el ejercicio anteriro.
Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp2019_tarea_ud06.git
```

## **Descripción y pruebas del ejercicio 6.2.**

Para firmar digitalmente la aplicación anterior, lo hago a través de la consola siguiendo los pasos indicados en el temario, haciendo uso de los comandos keytool para crear las claves publica y privada y jarsigner para firmar el fichero jar.

Pero antes obtenjo en jar inicial a través del IDE

![paso5.PNG](readme_src/paso5.PNG)


Los comandos empleados quedarian:

>  Generar las claves publica y privada para firmar el fichero. 
>  Con el comando keytool

```
 keytool -genkey -alias tarea06 -keypass 111111 -keystore DAM -storepass paperezsi
```

>  Firmo el fichero jar original (Se debe espeficiar la contraseña storepass y el keypass del alias tarea06 tras ejecutar el comando). 
>  A través del comando jarsigner

```
 jarsigner -keystore DAM -signedjar psp06_tarea_certificada.jar psp06_tarea.jar tarea06
```

![paso6.PNG](readme_src/paso6.PNG)

>  Para terminar con el certificado exporto la llave pública del certificado.
>  Con el comando keytool

```
 keytool -export -keystore DAM -alias tarea06 -file clavePublicaTarea06.cert
```

![paso8.PNG](readme_src/paso8.PNG)


Debemos importar el certificado en el equipo donde se ejecute la aplicación. En este caso el almacen DAM ya tiene el certificado y por tando nos lanza un mensaje de que son identicos.

```
 keytool -import -alias tarea06 -file clavePublicaTarea06.cert -keystore DAM
```

![paso9.PNG](readme_src/paso9.PNG)

Después ejecutando el comando "policytool" en la herramienta de politicas le doy a la opción de Almacén de claves, donde especificamos la url y el tipo de clave:

```
 policytool
```

![paso10.PNG](readme_src/paso10.PNG)

Trás haber especificado la url donde se encuentra el keystore que contiene el certificado, es cuando especifico las directivas de seguridad para que solo permita leer ficheros desde el directorio indicado "c:datos", para ello, en la misma herramienta, doy a la opción de agregar entrada de política, especificando el nombre del certificado "tarea06" que se lee del keystore indicado anteriormente, y especifico un tipo de permiso de FilePermission, porque es de acceso a un fichero y el directorio y la acción.

![paso11.PNG](readme_src/paso11.PNG)

Finalmente ejecuto la aplicación con el segurity.manager

```
java -Djava.security.manager -cp psp06_tarea_certificada.jar psp06_tarea
```


## Meta

Pablo Pérez – ue57919@edu.xunta.es

Distribuido bajo la licencia CC BY. Ver [``LICENCIAS``](https://creativecommons.org/licenses/?lang=es_ES) para más información.

[https://bitbucket.org/paperezsi/](https://bitbucket.org/paperezsi/)